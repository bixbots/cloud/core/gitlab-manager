# gitlab-manager

## Summary

Creates a GitLab CI/CD manager that uses docker-machine to provision EC2 instances on-demand that perform the actual builds using docker.

See the [gitlab-manager](https://gitlab.com/bixbots/cloud/terraform-modules/gitlab-manager) terraform module for more details.

## Configuration

| Name                  | Value                               |
|:----------------------|:------------------------------------|
| environment           | `bixbots`                           |
| manager_instance_type | `t3.nano`                           |
| manager_spot_price    | `0.0052`                            |
| runner_instance_type  | `t3.medium`                         |
| runner_spot_price     | `0.0416`                            |
| ssh_public_key        | `gitlab-bixbots-manager.pubkey.pem` |
