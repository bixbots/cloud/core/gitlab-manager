provider "aws" {
  region = "us-east-1"
}

terraform {
  required_version = "1.3.8"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.54.0"
    }
  }

  backend "s3" {
    region         = "us-east-1"
    dynamodb_table = "terraform-lock"
    encrypt        = "true"

    bucket = "bixbots-terraform-state"
    key    = "core/gitlab-manager"
  }
}
