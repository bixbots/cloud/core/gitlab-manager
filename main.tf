# gitlab-manager.bixbots.cloud

module "gitlab_manager" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/gitlab-manager.git?ref=v1"

  environment = "bixbots"

  manager_instance_type = "t3.nano"
  manager_spot_price    = "0.0052"

  runner_instance_type = "t3.medium"
  runner_spot_price    = "0.0416"

  ssh_public_key = file("${path.module}/files/gitlab-bixbots-manager.pubkey.pem")

  tags = {
    "TerraformModule" = "core/gitlab-manager"
  }
}
